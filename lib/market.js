const _ = require('lodash')
const WebSocket = require('ws')
const EventEmitter = require('events')
const Pair = require('./pair')
const Finder = require('./finder')

module.exports = class Market extends EventEmitter {
  constructor () {
    super()

    this.finder = new Finder()
    this.pairs = {}

    this.listenToTickSocket()
    this.listenToOpportunitiesUpdates()
  }

  listenToTickSocket () {
    const ws = new WebSocket('wss://stream.binance.com:9443/ws/!ticker@arr')
    ws.on('message', message => this.handleMessage(message))
  }

  handleMessage (message) {
    let data = JSON.parse(message)
    data.forEach(tick => this.updatePair(tick.s, tick))

    this.finder.findOpportunities(this.pairs)

    this.broadcastMarketUpdate()
    this.broadcastOpportunitiesUpdate()
  }

  updatePair (symbol, streamData) {
    if (this.pairs[symbol]) {
      this.pairs[symbol].update(streamData)
    } else {
      this.pairs[symbol] = new Pair(streamData)
    }
  }

  listenToOpportunitiesUpdates () {
    this.finder.on('opportunities-update', () => { this._hasUpdates = true })
  }

  broadcastMarketUpdate () {
    this.emit('market-update', Object.values(this.pairs))
  }

  broadcastOpportunitiesUpdate () {
    if (this._hasUpdates) {
      let topOpportunities = _.orderBy(this.finder.opportunities, ['margin'], ['desc']).slice(0, 30)
      this.emit('opportunities-update', topOpportunities)
      this._hasUpdates = false
    }
  }
}
