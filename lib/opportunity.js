const EventEmitter = require('events')

module.exports = class Opportunities extends EventEmitter {
  constructor (transactions) {
    super()

    this.transactions = transactions
    this._margin = 0
    this.key = this.setKey()
  }

  get margin () {
    return this._margin
  }

  set margin (value) {
    if (this._margin !== value) {
      this._margin = value
      this.emit('margin-update', this)
    }
  }

  setKey () {
    return this.transactions
      .map(transaction => transaction.pair.symbol)
      .join(' -> ')
  }

  calcMargin () {
    let margin = 100

    for (let transaction of this.transactions) {
      margin *= (transaction.action === 'buy'
        ? (1 / transaction.pair.price) // Buy => amount / price
        : transaction.pair.price) // Sell => amount * price
    }

    this.margin = Math.round((margin - 100) * 1000) / 1000 // Calc profit
  }

  listenToPairChanges () {
    this.transactions.forEach(transaction => transaction.pair.on('pair-update', () => this.calcMargin()))
  }
}
