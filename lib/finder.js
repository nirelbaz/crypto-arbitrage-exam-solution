const _ = require('lodash')
const Opportunity = require('./opportunity')
const Transaction = require('./transaction')
const EventEmitter = require('events')

module.exports = class Finder extends EventEmitter {
  constructor (maxLength = 3, baseCurrency = 'BNB') {
    super()

    this.maxLength = maxLength
    this.baseCurrency = baseCurrency
    this.opportunities = {}
  }

  findOpportunities (pairs) {
    let groupByAction = this.groupPairsByAction(pairs)

    for (let action in groupByAction) {
      groupByAction[action].forEach(pair => this.findNextStep(pairs, [new Transaction(pair, action)]))
    }
  }

  groupPairsByAction (pairs) {
    return Object.values(pairs).reduce((result, pair) => {
      if (pair.currency === this.baseCurrency) { result.buy.push(pair) } else
      if (pair.coin === this.baseCurrency) { result.sell.push(pair) }

      return result
    }, {buy: [], sell: []})
  }

  findNextStep (pairs, path) {
    if (this.isCompleteCycle(path)) { return }

    let lastTransaction = path[path.length - 1]

    _(pairs)
      .map(pair => (new Transaction(pair, this.getNextPairActionType(lastTransaction, pair))))
      .filter(transaction => transaction.action)
      .forEach(transaction => {
        if (path.indexOf(transaction.pair) > -1) { return } // No loops

        let forkPath = _.clone(path)
        forkPath.push(transaction)
        this.findNextStep(pairs, forkPath)
      })
  }

  getNextPairActionType (lastTransaction, connectionPair) {
    let currentCoin = lastTransaction.action === 'buy'
    ? lastTransaction.pair.coin
    : lastTransaction.pair.currency

    if (lastTransaction.pair === connectionPair) { return false } else
    if (currentCoin === connectionPair.coin) { return 'sell' } else
    if (currentCoin === connectionPair.currency) { return 'buy' }

    return false
  }

  isCompleteCycle (path) {
    if (path.length > this.maxLength) { return true } // Length limit

    let lastTransaction = path[path.length - 1]

    // Complete cycle
    if ((lastTransaction.pair.currency === this.baseCurrency && lastTransaction.action === 'sell') ||
      (lastTransaction.pair.coin === this.baseCurrency && lastTransaction.action === 'buy')) {
      this.addOpportunity(path)
      return true
    }

    return false
  }

  addOpportunity (path) {
    let opportunity = new Opportunity(path)

    if (!this.opportunities[opportunity.key]) {
      opportunity.calcMargin()
      opportunity.listenToPairChanges()
      opportunity.on('margin-update', () => this.emit('opportunities-update', this.opportunities))

      this.opportunities[opportunity.key] = opportunity
    }
  }
}
